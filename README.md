1. 分支 

   查看分支: git branch
   
   创建分支: git branch name
   
   切换分支: git checkout name
   
   创建+切换分支：git checkout -b name
   
   删除分支：git branch -d name


2. 更新本地文件 

   git pull
   

3. 查看状态 

    git status
    

4. 添加文件 

   添加单个： git add 文件名
   
   添加所有： git add .


5. 提交修改 

   git commit -m "记录修改的内容"


6. 推送到远程仓库 

   git push origin 远程仓库链接/分支名
   
   git push


7. 在本地仓库删除指定文件 

   git rm 文件名名称


8. 在本地仓库删除指定文件夹 

   git rm -r 文件夹/


   

   

   

    

